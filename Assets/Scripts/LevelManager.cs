﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    public Sprite appleSprite;
    public Sprite pearSprite;
    public Sprite bananaSprite;
    public Sprite orangeSprite;

    public GameObject fruitPrefab; 

    public Column[] columns;

    GameObject[] hexArray;

    static LevelManager _instance;
    public static LevelManager instance
    {
        get
        {
            if (!_instance)
                _instance = Object.FindObjectOfType(typeof(LevelManager)) as LevelManager;
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    void Start ()
    {
        InitColumns();
	}
    /*
    public void UpdateHexes()
    {
        for
    }*/

    void InitColumns()
    {
        for (int i = 0; i < columns.Length; i++)
        {
            columns[i].InitFruits();
        }
    }
}
