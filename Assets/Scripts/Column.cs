﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column : MonoBehaviour {

    public Hex[] hexes;

    public void InitFruits()
    {
        for(int i = 0; i< hexes.Length; i++)
        {
            GenerateNewFruit(i);
        }
    }

    void GenerateNewFruit(int num)
    {
        Fruit fruit = Instantiate(LevelManager.instance.fruitPrefab).GetComponent<Fruit>();
        fruit.fruitType = (FruitType)Random.Range(0, System.Enum.GetValues(typeof(FruitType)).Length - 1);
        //TODO: Set position
        //TODO: Set target pos
    }
}
